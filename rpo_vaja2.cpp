#include <iostream>
#include <stdlib.h> 
#include <stdio.h>
#include <list>
#include <algorithm>
#define NIL -1


int max(int a, int b) //Utility function to get max of 2 integers
{
	return (a > b) ? a : b;
}

int lcs(char* X, char* Y, int m, int n) //Returns length of LCS for X[0..m-1], Y[0..n-1]
{
	if (m == 0 || n == 0)
		return 0;
	if (X[m - 1] == Y[n - 1])
		return 1 + lcs(X, Y, m - 1, n - 1);
	else
		return max(lcs(X, Y, m, n - 1), lcs(X, Y, m - 1, n));
}

class Graph // A class that represents an undirected graph 
{
	int V;    // No. of vertices 
	std::list<int>* adj;    // A dynamic array of adjacency lists 
	void bridgeUtil(int v, bool visited[], int disc[], int low[],
		int parent[]);
public:
	Graph(int V);   // Constructor 
	void addEdge(int v, int w);   // to add an edge to graph 
	void bridge();    // prints all bridges 
};

Graph::Graph(int V)
{
	this->V = V;
	adj = new std::list<int>[V];
}

void Graph::addEdge(int v, int w)
{
	adj[v].push_back(w);
	adj[w].push_back(v);  // Note: the graph is undirected 
}

/* A recursive function that finds and prints bridges using 
DFS traversal. u --> The vertex to be visited next 
visited[] --> keeps tract of visited vertices 
disc[] --> Stores discovery times of visited vertices 
parent[] --> Stores parent vertices in DFS tree */
void Graph::bridgeUtil(int u, bool visited[], int disc[],
	int low[], int parent[])
{
	// A static variable is used for simplicity, we can  
	// avoid use of static variable by passing a pointer. 
	static int time = 0;

	// Mark the current node as visited 
	visited[u] = true;

	// Initialize discovery time and low value 
	disc[u] = low[u] = ++time;

	// Go through all vertices aadjacent to this 
	std::list<int>::iterator i;
	for (i = adj[u].begin(); i != adj[u].end(); ++i)
	{
		int v = *i;  // v is current adjacent of u 

		// If v is not visited yet, then recur for it 
		if (!visited[v])
		{
			parent[v] = u;
			bridgeUtil(v, visited, disc, low, parent);

			// Check if the subtree rooted with v has a  
			// connection to one of the ancestors of u 
			low[u] = std::min(low[u], low[v]);

			// If the lowest vertex reachable from subtree  
			// under v is  below u in DFS tree, then u-v  
			// is a bridge 
			if (low[v] > disc[u])
				std::cout << u << " " << v << std::endl;
		}

		// Update low value of u for parent function calls. 
		else if (v != parent[u])
			low[u] = std::min(low[u], disc[v]);
	}
}

void Graph::bridge() // DFS based function to find all bridges. It uses recursive function bridgeUtil() 
{
	// Mark all the vertices as not visited 
	bool* visited = new bool[V];
	int* disc = new int[V];
	int* low = new int[V];
	int* parent = new int[V];

	// Initialize parent and visited arrays 
	for (int i = 0; i < V; i++)
	{
		parent[i] = NIL;
		visited[i] = false;
	}

	// Call the recursive helper function to find Bridges 
	// in DFS tree rooted with vertex 'i' 
	for (int i = 0; i < V; i++)
		if (visited[i] == false)
			bridgeUtil(i, visited, disc, low, parent);
}

void merge(int arr[], int l, int m, int r) // Merges two subarrays of arr[]. First subarray is arr[l..m] Second subarray is arr[m+1..r]
{
	int i, j, k;
	int n1 = m - l + 1;
	int n2 = r - m;
	int* L = new int[n1]; //create temp arrays
	int* R = new int [n2];

	/* Copy data to temp arrays L[] and R[] */
	for (i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (j = 0; j < n2; j++)
		R[j] = arr[m + 1 + j];

	/* Merge the temp arrays back into arr[l..r]*/
	i = 0; // Initial index of first subarray 
	j = 0; // Initial index of second subarray 
	k = l; // Initial index of merged subarray 
	while (i < n1 && j < n2)
	{
		if (L[i] <= R[j])
		{
			arr[k] = L[i];
			i++;
		}
		else
		{
			arr[k] = R[j];
			j++;
		}
		k++;
	}

	while (i < n1) // Copy the remaining elements of L[], if there are any
	{
		arr[k] = L[i];
		i++;
		k++;
	}

	while (j < n2) //Copy the remaining elements of R[], if there are any
	{
		arr[k] = R[j];
		j++;
		k++;
	}
}
void printArray(int A[], int size) //Function to print an array
{
	int i;
	for (i = 0; i < size; i++)
		printf("%d ", A[i]);
	printf("\n");
}


void mergeSort(int arr[], int l, int r) //l is for left index and r is right index of the sub - array of arr to be sorted
{
	if (l < r)
	{
		int m = l + (r - l) / 2; //Same as (l+r)/2, but avoids overflow for large l and h 

		// Sort first and second halves 
		mergeSort(arr, l, m);
		mergeSort(arr, m + 1, r);

		merge(arr, l, m, r);
	}
}


int main()
{
	char meni = '0';
	do {
		std::cout << "1) Merge sort" << std::endl;
		std::cout << "2) Bridges in a graph" << std::endl;
		std::cout << "3) Longest common subsequence" << std::endl;
		std::cout << "0) Izhod" << std::endl;
		std::cout << "Vnesite vaso izbiro: ";
		std::cin >> meni;
		std::cout << std::endl;
		switch (meni)
		{
		case '1':
		{
			int arr[] = { 12, 11, 13, 5, 6, 7 };
			int arr_size = sizeof(arr) / sizeof(arr[0]);

			printf("Given array is \n");
			printArray(arr, arr_size);

			mergeSort(arr, 0, arr_size - 1);

			printf("\nSorted array is \n");
			printArray(arr, arr_size);
			break;
		}
		case '2':
		{
			std::cout << "\nBridges in first graph \n"; // Create graphs given in above diagrams 
			Graph g1(5);
			g1.addEdge(1, 0);
			g1.addEdge(0, 2);
			g1.addEdge(2, 1);
			g1.addEdge(0, 3);
			g1.addEdge(3, 4);
			g1.bridge();

			std::cout << "\nBridges in second graph \n";
			Graph g2(4);
			g2.addEdge(0, 1);
			g2.addEdge(1, 2);
			g2.addEdge(2, 3);
			g2.bridge();

			std::cout << "\nBridges in third graph \n";
			Graph g3(7);
			g3.addEdge(0, 1);
			g3.addEdge(1, 2);
			g3.addEdge(2, 0);
			g3.addEdge(1, 3);
			g3.addEdge(1, 4);
			g3.addEdge(1, 6);
			g3.addEdge(3, 5);
			g3.addEdge(4, 5);
			g3.bridge();
			break;
		}
		case '3':
		{
			char X[] = "AGGTAB";
			char Y[] = "GXTXAYB";

			int m = strlen(X);
			int n = strlen(Y);

			std::cout << "Length of LCS is " << lcs(X, Y, m, n) << std::endl;
			break;
		}
		default:
		{
			meni = '0';
			std::cout << "KONEC" << std::endl;
			break;
		}
		}
		std::cout << std::endl;
	} while (meni != '0');
}